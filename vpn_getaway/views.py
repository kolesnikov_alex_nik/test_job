from django.shortcuts import render, redirect
from django.http import HttpResponse, JsonResponse
from django.views.decorators.csrf import csrf_exempt
from rest_framework.parsers import JSONParser
from .models import *
from .serializers import CompanySerializer
import datetime
import random
import time
import calendar

@csrf_exempt
def company_list(request):
    if request.method == 'GET':
        company = Company.objects.all()
        serializer = CompanySerializer(company, many=True)
        return JsonResponse(serializer.data, safe=False)

    elif request.method == 'POST':
        data = JSONParser().parse(request)
        serializer = CompanySerializer(data=data)
        if serializer.is_valid():
            serializer.save()
            company = Company.objects.all()
            serializer_all = CompanySerializer(company, many=True)
            return JsonResponse(serializer_all.data, safe=False)
        return JsonResponse(serializer.errors, status=400)


@csrf_exempt
def company_detail(request, pk):
    try:
        company = Company.objects.get(pk=pk)
    except Company.DoesNotExist:
        return HttpResponse(status=404)

    if request.method == 'GET':
        serializer = CompanySerializer(company)
        return JsonResponse(serializer.data)

    elif request.method == 'PUT':
        data = JSONParser().parse(request)
        serializer = CompanySerializer(company, data=data)
        if serializer.is_valid():
            serializer.save()
            company = Company.objects.all()
            serializer_all = CompanySerializer(company, many=True)
            return JsonResponse(serializer_all.data, safe=False)
        return JsonResponse(serializer.errors, status=400)

    elif request.method == 'DELETE':
        company.delete()
        company = Company.objects.all()
        serializer = CompanySerializer(company, many=True)
        return JsonResponse(serializer.data, safe=False)


def generate_transfer(request):
    users = Users.objects.all()
    month_in_sec = 60 * 60 * 24 * 30
    now_time = int(time.time())
    for user in users:
        transfers_count = random.randrange(50, 501)
        month_index = 1
        for count in range(1, transfers_count):
            tr = random.randint(100, 10000000000)
            random_date = random.randrange(now_time - month_in_sec * month_index, now_time - (month_in_sec * (month_index-1)))
            transaction = Transactions(transfer=tr, date=datetime.datetime.fromtimestamp(random_date, datetime.timezone.utc), user=user)
            transaction.save()
            month_index += 1
            if month_index == 7:
                month_index = 1
    return HttpResponse('done')

@csrf_exempt
def clean_transfer(request):
    if request.method == 'DELETE':
        Transactions.objects.all().delete()
        return JsonResponse('all delete', safe=False)
    return redirect('/')


def show_quota_company(request):
    companies = Company.objects.all()
    # transfers_last = Transactions.objects.latest()
    # transfers_first = Transactions.objects.earliest()
    total_company = []
    for company in companies:
        qouta = 0
        users = company.users_company.all()
        for user in users:
            transfers = user.transfer_users.filter(date__range=["2020-03-01", "2020-03-31"])
            for transfer in transfers:
                qouta += transfer.transfer
        total_company.append({'company': company.name, 'company_qouta': company.quota, 'qouta': qouta})
    if request.method == 'GET':
        date_request = str(request.GET.get('date')).split('-')
        moth_request = date_request[0]
        year_request = date_request[1]
        days = calendar.monthrange(int(year_request), int(moth_request))
        total_company = []
        for company in companies:
            qouta = 0
            users = company.users_company.all()
            for user in users:
                transfers = user.transfer_users.filter(date__range=[f"{year_request}-{moth_request}-01",
                                                                    f"{year_request}-{moth_request}-{days[1]}"])
                for transfer in transfers:
                    qouta += transfer.transfer
            if qouta > company.quota:
                total_company.append({'company': company.name, 'company_qouta': company.quota, 'qouta': qouta})
        # serializer = CompanySerializer(company, many=True)
        return JsonResponse(total_company, safe=False)

    return render(request, 'show_quota_company.html', {'total_companies': total_company})