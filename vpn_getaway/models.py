from django.db import models


class Company(models.Model):
    name = models.CharField('Название', max_length=20)
    quota = models.BigIntegerField('Квота')

    class Meta:
        verbose_name = "Компания"
        verbose_name_plural = "Компании"

    def __str__(self):
        return self.name


class Users(models.Model):
    name = models.CharField('имя', max_length=20)
    email = models.EmailField('Email', max_length=255)
    company = models.ForeignKey(Company, on_delete=models.SET_NULL, null=True, verbose_name='Компания', related_name='users_company')

    class Meta:
        verbose_name = "Пользователь"
        verbose_name_plural = "Пользователи"

    def __str__(self):
        return self.name


class Transactions(models.Model):
    transfer = models.BigIntegerField('Трафик')
    date = models.DateTimeField('Дата',)
    user = models.ForeignKey(Users, on_delete=models.CASCADE, verbose_name ='Пользователь', related_name ='transfer_users')
    class Meta:
        get_latest_by = "date"


