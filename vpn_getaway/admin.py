from django.contrib import admin
from .models import *

admin.site.register(Company)


@admin.register(Transactions)
class TransactionsAdmin(admin.ModelAdmin):
    list_display = ('date', 'user', 'transfer')
    list_filter = ('user', 'date')

@admin.register(Users)
class UsersAdmin(admin.ModelAdmin):
    list_display = ('name', 'email', 'company')
    list_filter = ('company',)